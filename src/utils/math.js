export function lerp(min, max, t) {
  return min * (1 - t) + max * t;
}

export function damp(a, b, lambda) {
  return lerp(a, b, 1 - Math.exp(-lambda));
}

export function clamp(value, min, max) {
  return Math.min(Math.max(value, min), max);
}

export function distanceBetween(point1, point2) {
  return Math.sqrt(
    Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2)
  );
}

export function angleBetween(point1, point2) {
  return Math.atan2(point2.x - point1.x, point2.y - point1.y);
}
