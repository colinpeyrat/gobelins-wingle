import { TweenMax } from 'gsap';
import {
  Scene,
  OrthographicCamera,
  WebGLRenderer,
  MeshBasicMaterial,
  AxesHelper
} from 'three';

import { getViewport } from './utils/screen';
import { damp, clamp } from '@/utils/math';
import './controls/OrbitControls';
import './style.scss';

class WebGL {
  constructor({ $el, width, height }) {
    this.width = width;
    this.height = height;

    // clamp delta to stepping anything too far forward
    this.maxDeltaTime = 1 / 30;

    this.time = 0;
    this.lastTime = Date.now();

    this.frustumSize = 500;
    this.aspect = width / height;

    this.scene = new Scene();
    this.camera = this.createCamera();

    this.renderer = new WebGLRenderer({
      canvas: $el,
      antialias: true,
      alpha: false,
      failIfMajorPerformanceCaveat: true
    });
    this.renderer.setClearColor(0xffffff);

    this.controls = this.createControls();

    this.$canvas = $el;

    this.scene.add(this.createDebug());

    const viewport = getViewport(this.frustumSize, this.aspect);

    // create object like this
    // this.header = new Header(viewport.width, viewport.height);
    // this.scene.add(this.header);

    this.renderer.setSize(width, height);

    this.bindEvents();
    this.update();
  }

  bindEvents() {
    this.$canvas.addEventListener('mousemove', e => {
      const pos = {
        x: e.clientX / this.width,
        y: 1 - e.clientY / this.height
      };

      this.traverse('onMouseMove', e, pos);
    });
  }

  createDebug() {
    const axesHelper = new AxesHelper(125);

    return axesHelper;
  }

  createCamera() {
    const { width, height, frustumSize, aspect } = this;

    const camera = new OrthographicCamera(
      frustumSize * aspect / -2,
      frustumSize * aspect / 2,
      frustumSize / 2,
      frustumSize / -2,
      0.1,
      1000
    );

    camera.position.z = 500;

    return camera;
  }

  createControls() {
    const controls = new THREE.OrbitControls(
      this.camera,
      this.renderer.domElement
    );

    return controls;
  }

  update() {
    requestAnimationFrame(() => this.update());

    const now = Date.now();
    const dt = Math.min(this.maxDeltaTime, (now - this.lastTime) / 1000);
    this.time += dt;
    this.lastTime = now;

    // recursively tell all child objects to update
    this.scene.traverse(obj => {
      if (typeof obj.update === 'function') {
        obj.update(dt, this.time);
      }
    });

    this.controls.update();

    this.renderer.render(this.scene, this.camera);
  }

  traverse(fn, ...args) {
    this.scene.traverse(child => {
      if (typeof child[fn] === 'function') {
        child[fn].apply(child, args);
      }
    });
  }
}

export default WebGL;
