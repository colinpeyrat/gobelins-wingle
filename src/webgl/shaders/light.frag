#ifdef GL_ES
precision mediump float;
#endif

#pragma glslify: cnoise3 = require(glsl-noise/classic/3d)

varying vec2 vUv;

uniform float uTime;
uniform float uFlowSpeed;
uniform float uDisplacementOffset;
uniform bool uUseDisplacement;

uniform bool uDebugNoise;
uniform float uNoiseSpeed;
uniform float uNoiseScaleX;
uniform float uNoiseScaleY;
uniform float uNoiseAmplitude;

uniform vec2 uResolution;

uniform sampler2D uTexture;
uniform sampler2D uFlowMap;
uniform sampler2D uDisplacementMap;
uniform sampler2D uTrailTexture;

vec2 correctAspect(vec2 uv) {
  uv.x *= uResolution.x / uResolution.y;

  return uv;
}

vec3 getTrailColor(vec2 position) {
  vec3 color = texture2D(uTrailTexture, position).rgb;
  float backgroundTreshold = 0.05;

  if(color.r <= backgroundTreshold) {
    color = vec3(0.0, 0.0, 0.0);
  }

  return color;
}

vec2 applyTrail(vec2 position, vec3 trail) {
  float displacement = trail.r;
  displacement = 1.0 - displacement;

  position *= displacement;

  return position;
}

float applyNoise(vec2 uv) {
  float x = uv.x * uNoiseScaleX;
  float y = uv.y * uNoiseScaleY;

  float n = cnoise3(vec3(x, y, uTime * uNoiseSpeed));
  n *= uNoiseAmplitude;

  return n;
}

vec2 getFlowDir(vec2 st) {
  vec2 flowDir = texture2D(uFlowMap, st).rg;

  flowDir -= vec2(0.5, 0.5);

  return flowDir;
}

vec3 flow(vec2 st) {
  float phase1 = fract(uTime * uFlowSpeed + 0.5);
  float phase2 = fract(uTime * uFlowSpeed + 1.0);

  vec2 flowDir = getFlowDir(st);

  vec3 trail = getTrailColor(st);
  flowDir = applyTrail(flowDir, trail);
  /* vec3 flow = flow(displacement(applyTrail(position, trail))); */

  // mirroring phase
  phase1 = 1.0 - phase1;
  phase2 = 1.0 - phase2;

  vec2 noiseUv = st + applyNoise(st);

  vec3 color1 = texture2D(
      uTexture,
      noiseUv + flowDir * phase1).rgb;

  vec3 color2 = texture2D(
      uTexture,
      noiseUv + flowDir * phase2).rgb;

  float flowLerp = abs((0.5 - phase1) / 0.5);
  vec3 finalColor = mix(color1, color2, flowLerp);

  return finalColor;
}

vec2 displacement(vec2 uv) {
  if(uUseDisplacement) {
    float displacement = texture2D(uDisplacementMap, uv).r;
    displacement *= uDisplacementOffset;

    uv += vec2(0.0, displacement);
  }

  return uv;
}


float circle(in vec2 p, in float radius) {
    return length(p) * 2.0 - radius;
}


void main() {
	vec2 position = vUv;

  vec3 color;

  if(uDebugNoise) {
    color = vec3(applyNoise(position));
  } else {
    vec3 flow = flow(displacement(position));
    color = flow;
  }

  gl_FragColor = vec4(color, 1.0);
}
