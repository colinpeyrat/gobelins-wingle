export function getViewport(distance, aspect) {
  const height = distance;
  const width = height * aspect;

  return { width, height };
}
