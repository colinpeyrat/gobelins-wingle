import './style.scss';
import WebGL from '@/webgl';
import Intro from '@/components/Intro';

import { TweenMax } from 'gsap';

class App {
  constructor() {
    this.$el = document.querySelector('.App');
  }

  init() {
    // this.webGL = new WebGL({
    //   $el: this.$el.querySelector('.WebGL'),
    //   width: window.innerWidth,
    //   height: window.innerHeight
    // });

    this.intro = new Intro();
  }
}

export default new App();
