// import sono from 'sono';
import TweenMax from 'gsap/TweenMax';
import data from './../../data/data.json';

import './style.scss';

class Intro {
  constructor() {
    this.handleClick = this.handleClick.bind(this);
    this.handleEnd = this.handleEnd.bind(this);

    this.$el = document.querySelector('.Intro');
    this.$button = this.$el.querySelector('.Intro-button');
    this.$title = this.$el.querySelector('.Intro-title');
    this.$name = this.$el.querySelector('.Intro-name');
    this.$sprite = this.$el.querySelector('.Intro-explode');

    this.$button.addEventListener('click', this.handleClick);
  }

  handleClick(evt) {
    this.audio ? this.audio.pause() : void 0;
    const random = Intro.pickRandom(data);
    this.soundPath = `./assets/sounds/${random.sound}`;
    this.name = random.name;

    this.$name.textContent = this.name;

    this.audio = new Audio(this.soundPath);
    this.audio.volume = 0.4;
    this.audio.play();

    TweenMax.to(this.$button, 0.3, { x: 150, opacity: 0});
    TweenMax.to(this.$title, 0.3, { x: -150, opacity: 0});

    this.handleBars();

    // setTimeout(() => {
    //   this.handleEnd();
    // }, 2000);
    this.audio.onended = this.handleEnd;
  }

  static pickRandom(arr) {
    const min = 0;
    let max = arr.length - 1;
    let index = Math.floor(Math.random() * (max - min + 1) + min);
    const pick = arr[index];
    max = pick.sound.length - 1;
    index = Math.floor(Math.random() * (max - min + 1) + min);
    return {
      sound: pick.sound[index],
      name: pick.name
    };
  }

  handleEnd() {
    const timer = 2000;
    this.$sprite.classList.add('Intro-explode--active');

    TweenMax.to(this.$el.querySelectorAll('.Intro-bars'), 0.2, { autoAlpha: 0.4 });

    const audio = new Audio('./assets/explode.mp3');
    audio.volume = 1;
    audio.play();

    setTimeout(() => {
      TweenMax.set(this.$name, { display: 'block' });
    }, timer / 2);

    setTimeout(() => {
      this.$sprite.classList.remove('Intro-explode--active');
    }, timer);
  }

  handleBars() {
    TweenMax.set(this.$el.querySelectorAll('.Intro-bars'), { autoAlpha: 1 });
    const bars = this.$el.querySelectorAll('.Intro-bar');

    bars.forEach(($bar) => {
      this.animateBar($bar);
    });
  }

  animateBar($bar) {
    TweenMax.set($bar, {
      width: Intro.random(window.innerWidth * 0.05, window.innerWidth * 0.4),
    });

    setTimeout(() => {
      TweenMax.to($bar, Intro.random(0.25, 4) * Intro.random(0.8, 1), {
        x: window.innerWidth * 1.5, onComplete: () => {
          TweenMax.set($bar, { x: -window.innerWidth * 0.5 });
          this.animateBar($bar);
        }
      });
    }, Intro.random(0, 700));
  }

  static random(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}

export default Intro;
